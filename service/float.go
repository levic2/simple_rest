package service

import (
	"fmt"
	"strconv"
)

// MultiHundred : 傳入的數字乘 100 後回傳
func MultiHundred(a float64) float64 {
	r,_ := strconv.ParseFloat(fmt.Sprintf("%.2f", a * 100),64)
	return r
}
