package dbaccess

import (
	"log"
	"net/http"
	"simple_rest/api/protocol"
	"simple_rest/database"
	"simple_rest/env"

	"github.com/gin-gonic/gin"
)

type updateUserInput struct{
	Account string `form:"Account"`
	Password string `form:"Password"`
}

func Change(c *gin.Context){
	res := protocol.Response{}
	input := &updateUserInput{}
	r := resultOutput{IsOK:false}
	res.Result = &r

	if err := c.Bind(input); err != nil {
		log.Println(err)
		c.JSON(http.StatusBadRequest, r)
		return
	}

	if err:= UpdateUser(input); err != nil {
		c.JSON(http.StatusInternalServerError, protocol.SomethingWrongRes(err))
		return
	}

	r.IsOK = true
	c.JSON(http.StatusOK, res)
	return
}

func UpdateUser(data *updateUserInput) (err error){
	fn := "InsertUser"

	dbS := database.GetConn(env.AccountDB)

	sql := " UPDATE"
	sql += " user "
	sql += " SET "
	sql += " password = ? "
	sql += " WHERE account = ?;"

	_, err = dbS.Exec(sql, data.Password, data.Account)
	if err != nil {
		log.Fatalf("Exec Update Failed. fn:%s , error:%s", fn, err.Error())
		return
	}

	return
}