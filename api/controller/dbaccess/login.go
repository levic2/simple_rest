package dbaccess

import (
	"log"
	"net/http"
	"simple_rest/api/protocol"
	"simple_rest/database"
	"simple_rest/env"

	"github.com/gin-gonic/gin"
)

type loginUserInput struct{
	Account string `form:"Account"`
	Password string `form:"Password"`
}

func Login(c *gin.Context){
	res := protocol.Response{}
	input := &loginUserInput{}

	if err := c.Bind(input); err != nil {
		log.Println(err)
		c.JSON(http.StatusBadRequest, res)
		return
	}

	u, err:= VerifyUser(input);
	if err != nil {
		c.JSON(http.StatusInternalServerError, protocol.SomethingWrongRes(err))
		return
	}

	if u == nil {
		res.Code = 2
		res.Message = "Login Failed"
		res.Result = u
		c.JSON(http.StatusBadRequest, res)
		return
	}
	
	c.JSON(http.StatusOK, res)
	return
}

func VerifyUser(data *loginUserInput) (user *User, err error){
	fn := "VerifyUser"

	dbS := database.GetConn(env.AccountDB)

	sql := " SELECT "
	sql += " account, password "
	sql += " FROM user "
	sql += " WHERE "
	sql += " account = ? AND password = ? ;"

	rows, err := dbS.Query(sql, data.Account, data.Password)
	if err != nil {
		log.Fatalf("Exec Query Failed. fn:%s , error:%s", fn, err.Error())
		return
	}

	defer rows.Close()

	for rows.Next() {
		user = &User{}
		if err := rows.Scan(
			&user.Account,
			&user.Password,
		); err != nil {
			log.Fatalf("Fatch Data Error. fn:%s , err:%s", fn, err.Error())
			break
		}
	}

	return
}